from django.shortcuts import render, redirect
from .models import Users, List, Items
# Create your views here.
def index(request):
	if not 'errors' in request.session:
		request.session['errors'] = []
	context = {
	'users':Users.userManager.all()
	}
	return render(request, 'wish/index.html', context)


def register(request):
	if request.method == 'POST':
		result = Users.userManager.add(request.POST['first_name'], request.POST['last_name'], request.POST['email'], request.POST['password'], request.POST['confirm_password'])
		if result[0]:
			request.session['first_name'] = result[1].first_name
			request.session.pop('errors')
			return redirect('/wish_list')
		else:
			request.session['errors'] = result[1]
			return redirect('/')
	else:
		return redirect('/')


def login(request):
	result = Users.userManager.login(request)
	if result[0] == False:
		print "login failed"
		return redirect('/')

	return redirect('/wish_list')


def wish_list(request):
	context = {
	'name': request.session['first_name']
	}
	return render(request, 'wish/wish_list.html', context)


def create(request):

	return render(request, 'wish/create_item.html')	

def add_item(request):
	if request.method == 'POST':
		result = Items.userManager.create_item(item = request.session['item'])
		if result[0]:
			request.session['item'] = result[1].item
			request.session.pop('errors')
			return redirect('/wish_list')
		else:
			request.sessin['errors'] = result[1]
			return redirect('/create')


def logout(request):
	request.session.pop('first_name')
	return redirect('/')

def delete(request, id):
	request.session['current_row']=id
	row = Items.userManager.filter(id=request.session['current_row']).delete()
	return redirect('/wish_list')
