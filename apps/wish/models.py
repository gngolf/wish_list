
from __future__ import unicode_literals

from django.db import models
import re, bcrypt

EMAIL_REGEX = re.compile(r'^[a-zA-Z0-9\.\+_-]+@[a-zA-Z0-9\._-]+\.[a-zA-Z]*$')

NAME_REGEX = re.compile(r'^[a-zA-z.+_-]+$')


# Create your models here.
class UserManager(models.Manager):
	def add(self, first_name, last_name, email, password, confirm_password):
		errors=[]
		
		if len(first_name) == 0:
			errors.append('First name required')
		elif len(first_name) <= 2:
			errors.append('First name must be longer than 2 letters long')
		elif not NAME_REGEX.match(first_name):
			errors.append('First name must contain only letters')
		
		if len(last_name) == 0:
			errors.append('Last name required')
		elif len(last_name) <= 2:
			errors.append('Last name must be longer than 2 letters long')
		elif not NAME_REGEX.match(last_name):
			errors.append('Last name must contain only letters')
		
		elif len(email) == 0:
			errors.append('Email required')
		elif not EMAIL_REGEX.match(email):
			errors.append('Not a valid email')

		elif len(password) == 0:
			errors.append('Password required')
		elif len(password) < 8:
			errors.append('Password must be at least 8 characters in length')
		elif password != confirm_password:
			errors.append('Your password confirmation did not match your password')

		if len(errors) is not 0:
			return (False, errors)
		else:
			pw_hash = bcrypt.hashpw(password.encode(), bcrypt.gensalt())
			u = Users.userManager.create(first_name=first_name, last_name=last_name, email=email, pw_hash=pw_hash)
		
			return(True, u)

	def login(self, request):
		try:
			print request.POST['email']
			u = Users.userManager.get(id=id)
			password = request.POST['password'].encode()
			pw=u.pw_hash.encode()
			if bcrypt.hashpw(password, pw) == pw:
				return(True, u)
			
		except:
			pass
			
		return(False,)

	def create_item(self, request):
		errors = []
		if len(request.session['new_item']) < 3:
			errors.append('Item name must have at least 4 characters')
		if len(errors) is not 0:
			return (False, errors)
		else:
			i = Items.userManager.create(item = request.POST['item'])
			return(True, i)
			 





class Users(models.Model):
	first_name = models.CharField(max_length=100)
	last_name = models.CharField(max_length=100)
	email = models.CharField(max_length=200)
	pw_hash = models.CharField(max_length=255)
	userManager = UserManager()

class List(models.Model):
	user = models.ForeignKey(Users)
	item = models.CharField(max_length=255)
	created_at=models.DateTimeField(auto_now_add=True)
	userManager = UserManager()

class Items(models.Model):
	item = models.CharField(max_length=255)
	created_at = models.DateTimeField(auto_now_add=True)
	userManager = UserManager()
