from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^$', views.index),
    url(r'^register$', views.register),
    url(r'^login$', views.login),
    url(r'^wish_list$', views.wish_list),
    url(r'^create$', views.create),
    url(r'^add_item$', views.add_item),
    url(r'^logout$', views.logout),
    url(r'^delete/(?P<id>\d)$', views.delete)
]