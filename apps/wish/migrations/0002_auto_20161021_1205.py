# -*- coding: utf-8 -*-
# Generated by Django 1.10.2 on 2016-10-21 19:05
from __future__ import unicode_literals

from django.db import migrations
import django.db.models.manager


class Migration(migrations.Migration):

    dependencies = [
        ('wish', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelManagers(
            name='items',
            managers=[
                ('userManager', django.db.models.manager.Manager()),
            ],
        ),
        migrations.AlterModelManagers(
            name='list',
            managers=[
                ('userManager', django.db.models.manager.Manager()),
            ],
        ),
    ]
